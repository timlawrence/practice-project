import React from 'react';
import { connect } from 'react-redux';
import { increment, decrement } from '../actions';

class Counter extends React.Component {

  increment = () => {
    this.props.increment();
  }

  decrement = () => {
    this.props.decrement();
  }

  render() {
    return (
      <div>
        <h2>Over-Simplified Redux Example</h2>
        <div>
          <button onClick={this.decrement}>-</button>
          <span>{this.props.count}</span>
          <button onClick={this.increment}>+</button>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  increment: () => dispatch(increment()),
  decrement: () => dispatch(decrement())
});

function mapStateToProps(state) {
  return {
      count: state.count
  };
}

export default connect(mapStateToProps,mapDispatchToProps)(Counter);